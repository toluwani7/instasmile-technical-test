import React from "react";
import { CurrencyCalculator } from "./components/CurrencyCalculator";

function App() {
  return (
    <div className="App container mt-5">
      <CurrencyCalculator />
    </div>
  );
}

export default App;
