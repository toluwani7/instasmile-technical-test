import React, { useState, useEffect } from "react";
import styled from "styled-components";

const StyledWrapper = styled("div")`
  background-color: #e9ecef;
  padding: 20px;
  .subHeading {
    color: gray;
    font-size: 18px;
    margin-bottom: 15px;
  }
`;

export default () => {
  const [amount, setAmount] = useState(100);
  const [baseCurrencies, setBaseCurrencies] = useState([]);
  const [convertedCurrencies, setConvertedCurrencies] = useState([]);

  const [selectedBaseCurrency, setSelectedBaseCurrency] = useState("GBP");
  const [selectedConvertedCurrency, setSelectedConvertedCurrency] = useState(
    "USD"
  );

  const [resultText, setResultText] = useState(null);

  useEffect(() => {
    setBaseCurrencies(["USD", "EUR", "GBP"]);
    setConvertedCurrencies(["USD", "EUR", "GBP"]);
  }, []);

  const submitHandler = async () => {
    const url = `https://api.exchangerate-api.com/v4/latest/${selectedBaseCurrency}`;
    const request = await fetch(url);
    const json = await request.json();
    const { rates } = json;
    setResultText(
      `${amount} ${selectedBaseCurrency} = ${
        rates[selectedConvertedCurrency] * amount
      } ${selectedConvertedCurrency}`
    );
  };

  return (
    <StyledWrapper>
      <h3> Currency Calculator</h3>
      <div className="subHeading"> Convert the currency</div>
      <form className="form-inline">
        <div className="form-group mb-2">
          <label className="sr-only">amount</label>
          <input
            type="text"
            className="form-control"
            placeholder="amount"
            value={amount}
            onChange={(e) => setAmount(e.target.value)}
          />
        </div>
        <div className="form-group mx-sm-3 mb-2">
          <label className="sr-only">Currency</label>
          <select
            className="form-control"
            value={selectedBaseCurrency}
            onChange={({ target }) => setSelectedBaseCurrency(target.value)}
          >
            {baseCurrencies.map((currency, i) => {
              return (
                <option key={i} value={currency}>
                  {currency}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group mx-sm-3 mb-2">
          <label className="sr-only">Currency</label>
          <label>Convert to</label>
          <select
            className="form-control"
            value={selectedConvertedCurrency}
            onChange={({ target }) =>
              setSelectedConvertedCurrency(target.value)
            }
          >
            {convertedCurrencies.map((currency, i) => {
              return (
                <option key={i} value={currency}>
                  {currency}
                </option>
              );
            })}
          </select>
        </div>
        <button
          type="button"
          onClick={submitHandler}
          className="btn btn-primary mb-2"
        >
          Convert
        </button>
      </form>
      <div>{resultText && resultText}</div>
    </StyledWrapper>
  );
};
